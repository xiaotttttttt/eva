package com.eva.dao.system.vo;

import com.eva.dao.system.model.SystemPermission;
import com.eva.dao.system.model.SystemUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Data
@ApiModel("系统权限列表视图对象")
public class SystemPermissionListVO extends SystemPermission {

    @ApiModelProperty(value = "类型，module模块，permission权限")
    private String type;

    @ApiModelProperty(value = "模块路径")
    private String modulePath;

    @ApiModelProperty(value = "层级")
    private Integer level;

    @ApiModelProperty(value = "子权限列表")
    private List<SystemPermissionListVO> children;

    @ApiModelProperty(value = "创建人信息")
    private SystemUser createUserInfo;

    @ApiModelProperty(value = "更新人信息")
    private SystemUser updateUserInfo;
}
