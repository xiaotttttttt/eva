package com.eva.service.system;

import com.eva.core.model.PageData;
import com.eva.core.model.PageWrap;
import com.eva.dao.system.dto.QuerySystemRoleDTO;
import com.eva.dao.system.model.SystemRole;
import com.eva.dao.system.vo.SystemRoleListVO;

import java.util.List;

/**
 * 系统角色Service定义
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemRoleService {

    /**
     * 创建
     *
     * @param systemRole 实体
     * @return Integer
     */
    Integer create(SystemRole systemRole);

    /**
     * 主键删除
     *
     * @param id 主键
     */
    void deleteById(Integer id);

    /**
     * 批量主键删除
     *
     * @param ids 主键列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     *
     * @param systemRole 实体
     */
    void updateById(SystemRole systemRole);

    /**
     * 批量主键更新
     *
     * @param systemRoles 实体列表
     */
    void updateByIdInBatch(List<SystemRole> systemRoles);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return SystemRole
     */
    SystemRole findById(Integer id);

    /**
     * 根据用户ID查询
     *
     * @param userId 用户ID
     * @return List<SystemRole>
     */
    List<SystemRole> findByUserId(Integer userId);

    /**
     * 条件查询单条记录
     *
     * @param systemRole 查询条件
     * @return SystemRole
     */
    SystemRole findOne(SystemRole systemRole);

    /**
     * 条件查询
     *
     * @param systemRole 查询条件
     * @return List<SystemRole>
     */
    List<SystemRole> findList(SystemRole systemRole);

    /**
     * 分页查询
     *
     * @param pageWrap 分页参数
     * @return PageData<SystemRoleListVO>
     */
    PageData<SystemRoleListVO> findPage(PageWrap<QuerySystemRoleDTO> pageWrap);

    /**
     * 条件统计
     *
     * @param systemRole 统计参数
     * @return long
     */
    long count(SystemRole systemRole);
}
