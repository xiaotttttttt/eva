package com.eva.service.system;

import com.eva.dao.system.dto.LoginDTO;

import javax.servlet.http.HttpServletRequest;

/**
 * 系统登录
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemLoginService {

    /**
     * 密码登录
     *
     * @param dto 详见LoginDTO
     * @param request 请求对象
     * @return String
     */
    String loginByPassword (LoginDTO dto, HttpServletRequest request);
}
